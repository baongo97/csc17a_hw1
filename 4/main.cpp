/*
 *  Name: Bao Gia Ngo
 *  Student ID: 2843439 
 *  Date: 9/3/2019
 *  HW: 1.4
 *  
 * Problem: Write a program that dynamically 
 * allocates an array large enough to hold a user-defined 
 * number of test scores. Once all the scores are entered, 
 * the array should be passed to a function that sorts them 
 * in ascending order. Another function should be called that 
 * calculates the average score. The program should display the 
 * sorted list of scores and averages with appropriate headings. 
 * Use pointer notation rather than array notation whenever possible.
 * Input Validation: Do not accept negative numbers for test scores.
 *  
 * I certify this is my own work and code*/

#include <cstdlib>
#include <iostream>
#include <cmath>

using namespace std;

double average(double *scores, int nScores);
void sortTestCores(double *scores , int n);

int main()
{
    double *testScores;
    double avgScores = 0;
    int nScores, i;
    //prompt the user to enter number of test scores
    cout << "Enter number of test scores: ";
    cin >> nScores;
    //dynamically allocate memory to store test scores
    testScores = new double[nScores];
    //read all test scores
    for (i = 0; i < nScores; i++)
    {
        cout << "Enter Test Score " << i+1 << ": ";
        cin >> *(testScores + i);
        while (*(testScores + i) < 0)
        {
            cout << "Error! Only positive numbers"<<endl;
            cout << "Enter Test score " << i + 1 << ": ";
            cin >> *(testScores + i);         
        }
    }
    // call to sort test scores
    cout << "Sorted TestScores: "<<endl;
    for (i = 0; i<nScores; i++)
    {
        cout << *(testScores + i) << " ";
    }
    cout << endl;
    avgScores = average(testScores, nScores);
    cout << "Average of test scores is: " << avgScores << endl;

     return 0;
}

void sortTestScores(double *scores, int nScores)
{
    double temp;
    int i, j;
    //sort the scores
    for (i = 1; i<nScores; i++)
    {
        for (j = 0; j<nScores - i; j++)
        {
            if (scores[j]>scores[j + 1])
            {
                temp = scores[j];
                   scores[j] = scores[j + 1];
                   scores[j + 1] = temp;
             }
        }
    }
}

double average(double *score, int n)
{
    int i;
    double sum=0.0,avg = 0.0;
    //find the sum of all test scores
    for (i = 0; i<n; i++)
    {
        sum += score[i];
    }
    avg = sum/n;
    return avg;
}