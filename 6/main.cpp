/*
 *  Name: Bao Gia Ngo
 *  Student ID: 2843439 
 *  Date: 9/3/2019
 *  HW: 1.4
 *  
 * Problem: Modify problem 5 to read a set of test scores 
 * from a file as opposed to a user.
 * 
 * I certify this is my own work and code*/

#include <cstdlib>
#include <iostream>
#include <cmath>
#include <fstream>

using namespace std;

double average(double *scores, int nScores);
void sortTestCores(double *scores , int n);

int main()
{
    double *testScores;
    double avgScores = 0;
    int nScores, i;

    ifstream inFile;
    inFile.open("TestScore.txt");
    if (!inFile)
    {
        cout << "\nError opening file.\n";
        return 13;
    }
    while (inFile >> *testScores)
    {
        while (nScores > 0)
        {
            cout << nScores;
        }
    }
    inFile.close();
    
    // call to sort test scores
    cout << "Sorted TestScores: "<<endl;
    for (i = 0; i<nScores; i++)
    {
        cout << *(testScores + i) << " ";
    }
    cout << endl;
    avgScores = average(testScores, nScores);
    cout << "Average of test scores is: " << avgScores << endl;

     return 0;
}

void sortTestScores(double *scores, int nScores)
{
    double temp;
    int i, j;
    //sort the scores
    for (i = 1; i<nScores; i++)
    {
        for (j = 0; j<nScores - i; j++)
        {
            if (scores[j]>scores[j + 1])
            {
                temp = scores[j];
                   scores[j] = scores[j + 1];
                   scores[j + 1] = temp;
             }
        }
    }
}

double average(double *score, int n)
{
    int i;
    double sum=0.0,avg = 0.0;
    double lowest = score[0];
    //find the sum of all test scores
    for (i = 0; i<n; i++)
    {
        sum += score[i];
    }
    //Drop lowest score
    sum=sum - lowest;
    cout << "Lowest score " << lowest << " is droped from sum" << endl;
    //calculate average of test scores without lowest score
     avg = sum / (n-1);
    return avg;
}