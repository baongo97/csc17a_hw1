/*
 *  Name: Bao Gia Ngo
 *  Student ID: 2843439 
 *  Date: 9/3/2019
 *  HW: 1.2
 *  Problem: Typically, for a static array, we would have a third variable that stores the number of elements used. 
 *          However, dynamic arrays allow us to bypass this requirement.Create an array of 5 names. The names, or strings, 
 *          can be whatever you would like. Create a function that removes a certain string/name with a given location. 
 *          It may look like this.
 *          string* deleteEntry(string *array, int& size, int loc)
 *          The function will first create a new dynamic array, copy all the values except for one at the location given,
 *          and decrease the size. This will only be done if the location is actually valid, so proper error checking is needed.
 *          Finally, it will return the new dynamic array.Create a driver that tests if your function performed correctly.
 *  I certify this is my own work and code*/
#include <iostream>
#include <string>

using namespace std;

string* deleteEntry(string *array, int& size, int loc) 
{
   if(loc >= 0 && loc < size) 
   {
      string *newArray = new string[size];
      int index = 0;
      for(int i = 0; i < size; ++i) 
      {
         if(i != loc) {
            newArray[index++] = array[i];
         }
      }
      size--;
      return newArray;
   } else {
      return array;
   }
}

void print_array(string *arr, int size) 
{
   for(int i = 0; i < size; ++i) 
   {
      cout << arr[i] << " ";
   }
   cout << endl;
}

int main() 
{
   int size = 5;
   string *arr = new string[size];
   arr[0] = "tea";
   arr[1] = "cocacola";
   arr[2] = "milk";
   arr[3] = "coffee";
   arr[4] = "water";
   
   print_array(arr, size);
   arr = deleteEntry(arr, size, 3);   // delete coffee
   print_array(arr, size);
   arr = deleteEntry(arr, size, 2);   // delete milk
   print_array(arr, size);
   arr = deleteEntry(arr, size, 1);   // delete cocacola
   print_array(arr, size);
   arr = deleteEntry(arr, size, 6);  // invalid location because there only 5
   print_array(arr, size);
   return 0;
}