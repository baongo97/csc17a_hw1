/*
 *  Name: Bao Gia Ngo
 *  Student ID: 2843439 
 *  Date: 9/3/2019
 *  HW: 1.1
 *  Problem: Create a function that creates an array, one that stores random values into the array, 
 *           and one that outputs all the array values. Use a dynamic array for this problem.
 *  I certify this is my own work and code*/

#include <cstdlib>
#include <iostream>
#include <ctime>

using namespace std;

void storeArray(int *arr, int size) 
{
   for(int i = 0; i < size; ++i) 
   {
      arr[i] = rand() % 100;
   }
}

void outputArray(int *arr, int size) 
{
   cout << "Array is: ";
   for(int i = 0; i < size; ++i) {
      cout << arr[i] << " ";
   }
   cout << endl;
}

int main() 
{
   srand(time(NULL));
   int size = 10;
   int *arr = new int[size];
   storeArray(arr, size);
   outputArray(arr, size);
   delete[] arr;
   return 0;
}
