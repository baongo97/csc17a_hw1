/*
 *  Name: Bao Gia Ngo
 *  Student ID: 2843439 
 *  Date: 9/3/2019
 *  HW: 1.3
 *  Problem: Write a function named coinToss that simulates the tossing of a coin. 
 *      When you call the function, it should generate a random number in the range of 1 through 2. 
 *      If the random number is 1, the function should display “heads.” If the random number is 2, 
 *      the function should display “tails.” Demonstrate the function in a program that asks the user how 
 *      many times the coin should be tossed and then simulates the tossing of the coin that number of times.
 *  I certify this is my own work and code*/

#include <cstdlib>
#include <iostream>
#include <ctime>
#include <cmath>

using namespace std;
int coinToss()
{
    int value = rand()%2+1;
    switch (value)
    {
        case 1:
            cout << "Heads" << endl;
            break;
        default:
            cout << "Tails" << endl;  
            break;
    }
    return value;
}

int main()
{
    srand(time(NULL));
    int timeToss;
    
    cout << "Enter number of time to toss the coin: ";
    cin >> timeToss;
    int headTime, tailTime;
    
    for (int i = 0; i < timeToss; i++)
    {
        int coinTossed = coinToss();
        if (coinTossed == 1)
        {
            headTime++;
        }
        else if(coinTossed == 2)
        {
            tailTime++;
        }
    }
    cout <<"Total number of heads is " << headTime << endl;
    cout <<"Total number of tails is " << tailTime << endl;

    return 0;
}